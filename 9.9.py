cats = [False] * 100

for round_number in range(1, 101):
    for cat_number in range(round_number - 1, 100, round_number):
        cats[cat_number] = not cats[cat_number]

hat_count = 0
for cat_number, has_hat in enumerate(cats, start=1):
    if has_hat:
        print(f"Cat nr {cat_number} has a hat.")
        hat_count += 1

print(f"Total cats with hats: {hat_count}")

