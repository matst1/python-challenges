#zrobilem troche po swojemu, olalem tresc polecenia z ksiazki
#taka forma ma dla mnie wiekszy sens
#oczywiscie na gitcie jest wersja "z ksiazki"

import random
capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
}

random_state = random.choice(list(capitals_dict.keys()))

correct_capital = capitals_dict[random_state]

while True:
    capital = input(f'Type the correct capital of {random_state}: ')
    if capital.lower() == correct_capital.lower():
        print("Correct! That's the capital.")
        print("Thank You!")
        break
    else:
        print('Incorrect, try again')

