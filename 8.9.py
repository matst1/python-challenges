print('program podaje prawdopodobienstwo wygrania wyborów')
from random import random

wyg_a = 0
wyg_b = 0

for wyniki in range(0, 10_000):
    kan_a = 0
    kan_b = 0

    if random() < 0.87:
        kan_a = kan_a + 1
    else:
        kan_b = kan_b + 1

    if random() < 0.65:
        kan_a = kan_a + 1
    else:
        kan_b = kan_b + 1

    if random() < 0.17:
        kan_a = kan_a + 1
    else:
        kan_b = kan_b + 1

    if kan_a > kan_b:
        wyg_a = wyg_a + 1
    else:
        wyg_b = wyg_b + 1

print(f'prawdopodobienstwo ze A wygra to {wyg_a/10_000}')
print(f'prawdopodobienstwo ze B wygra to {wyg_b/10_000}')

    

