def cel_to_far(x):
    far = round(float(x) * 9/5 + 32, 2)
    return far

def far_to_cel(y):
    cel = round((float(y)-32)*5/9, 2)
    return cel

x = input("wpisz temperature w C: ")
y = input("wpisz temperature w F: ")
far1 = str(cel_to_far(x))
cel1 = str(far_to_cel(y))
print(x + " st. C " + "to " + far1 + " st. F")
print(y + " st. F " + "to " + cel1 + " st. C")
