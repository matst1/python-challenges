class Animal:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def eating(self, eat):
        if eat == 0:
            print("I eat meat,")
        elif eat == 1:
            print("I eat plants,")
        else:
            print("I eat meat and plants,")
        return ""

    def presentation(self):
        print(f"My name is {self.name}, I'm {str(self.age)} years old,")

    def jumping(self, jump=""):
        print(f'I jump {jump}')


class Dog(Animal):
    type = "Dog:"
    sound = "how how;"

    def voice(self):
        print(f'My voice is {self.sound}')


class Cat(Animal):
    type = "Cat:"
    sound = "meow meow,"

    def cat(self):
        dog_instance = Dog(self.name, self.age)
        dog_instance.sound = self.sound
        dog_instance.voice()

    def cat_jump(self, jump="high;"):
        return super().jumping(jump)

class Cow(Animal):
    type = "Cow:"
    sound = "muuu,"
    milk = "form me"

    def cow(self):
        dog_instance = Dog(self.name, self.age)
        dog_instance.sound = self.sound
        dog_instance.voice()

    def cow_jump(self, jump="high (if I'm mad:)),"):
       return super().jumping(jump)

    def cow_milk(self):
        return print(f"You get milk {self.milk}.")

print(f'')
dog = Dog("Lucek", 5)
print(dog.type)
dog.presentation()
dog.eating(3)
dog.voice()
print(f'')
cat = Cat("Bury", 6)
print(cat.type)
cat.presentation()
cat.eating(0)
cat.cat()
cat.cat_jump()
print(f'')
cow = Cow("Krasula", 9)
print(cow.type)
cow.presentation()
cow.eating(1)
cow.cow()
cow.cow_jump()
cow.cow_milk()
