import random

ns = random.sample(['fossil', 'horse', 'aardvark', 'judge',
         'chef', 'mango', 'extrovert', 'gorilla'], k=3)

vs = random.sample(['kicks', 'jingles', 'bounces', 'slurps',
         'meows', 'explodes', 'curdles'], k=3)

adj = random.sample(['furry', 'balding', 'incredulous', 'fragrant',
              'exuberant', 'glistening'], k=3)

ps = random.sample(['against', 'after', 'into', 'beneath', 'upon',
                'for', 'in', 'like', 'over', 'within'], k=2)

adv = random.choice(['curiously', 'extravagantly', 'tantalizingly', 'furiously',
           'sensuously'])

adj1 = adj[0]
adj2 = adj[2]

def is_vowel(char):
    vowels = "aeiouAEIOU"
    return char in vowels

if is_vowel(adj1[0]):
    art = "An "
else:
    art = "A "

if is_vowel(adj2[0]):
    art1 = "an "
else:
    art1 = "a "

print(f'{art}{adj[0]} {ns[0]} \n')
print(f'{art}{adj[0]} {ns[0]} {vs[0]} {ps[0]} the {adj[1]} {ns[1]}')
print(f'{adv}, the {ns[0]} {vs[1]}')
print(f'the {ns[1]} {vs[2]} {ps[1]} {art1}{adj[2]} {ns[2]}')
