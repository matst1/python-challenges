universities = [
    ['California', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['MIT', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]

def enrollment_stats(list_univer):
    total_stud = []
    total_fee = []
    for university in list_univer:
        total_stud.append(university[1])
        total_fee.append(university[2])
    return total_stud, total_fee

totals= enrollment_stats(universities)

def mean(values):
    return sum(values)/7

mean_stud = mean(totals[0])
mean_fee = mean(totals[1])

def median(values):
    values.sort()
    return values[3]

median_stud = median(totals[0])
median_fee = median(totals[1])

print('\n')
print('******' *6)
print(f'Total Students:  {sum(totals[0]):,}')
print(f'Total Tuition:  ${sum(totals[1]):,}')
print(f'\nStudent mean:   {mean(totals[0]):,.2f}')
print(f'Student median:  ${median(totals[0]):,}')
print(f'\nTuition mean:   {mean(totals[1]):,.2f}')
print(f'Tuition median:  ${median(totals[1]):,}')
print('******' *6)  
print('\n')
