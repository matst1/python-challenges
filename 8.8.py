import random

print("program podaje srednia ilosc rzutow monety \
jaka jest potrzebna do zmiany strony monety")

def fair_coin():
    if random.randint(0, 1) == 0:
        return "orzel"
    else:
        return "reszka"

proby = 10_000
obroty = 0


for trial in range(proby):
    if fair_coin() == "orzel":
        obroty = obroty + 1
        while fair_coin() == "orzel":
            obroty = obroty + 1 
    else:
        obroty = obroty + 1
        while fair_coin() == "reszka":
            obroty = obroty + 1
        obroty = obroty + 1 

srednia = obroty/proby
print(f'srednia to {srednia}')
